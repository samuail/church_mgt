FactoryBot.define do
  factory :sate_chmgt_church, parent: :sate_chmgt_lookup, class: 'Sate::Chmgt::Church' do
    type { 'Sate::Chmgt::Church' }
  end
end
