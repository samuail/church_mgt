FactoryBot.define do
  factory :sate_chmgt_lookup, class: 'Sate::Chmgt::Lookup' do
    name { FFaker::Name.name }
    type { 'Lookup' }
    application_module { create (:sate_auth_application_module) }
  end
end
