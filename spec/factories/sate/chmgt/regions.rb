FactoryBot.define do
  factory :sate_chmgt_region, parent: :sate_chmgt_lookup, class: 'Sate::Chmgt::Region' do
    type { 'Sate::Chmgt::Region' }
  end
end
