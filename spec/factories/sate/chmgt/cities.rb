FactoryBot.define do
  factory :sate_chmgt_city, parent: :sate_chmgt_lookup,class: 'Sate::Chmgt::City' do
    type { 'Sate::Chmgt::City' }
  end
end
