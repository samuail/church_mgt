FactoryBot.define do
  factory :sate_chmgt_country, parent: :sate_chmgt_lookup, class: 'Sate::Chmgt::Country' do
    type { 'Sate::Chmgt::Country' }
  end
end
