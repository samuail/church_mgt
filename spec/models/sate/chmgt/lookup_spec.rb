require 'rails_helper'

RSpec.describe Sate::Chmgt::Lookup, type: :model do
  it 'has a valid factory' do
    expect(create(:sate_chmgt_lookup)).to be_valid
  end

  it 'is invalid with no name' do
    expect(build(:sate_chmgt_lookup, :name => nil)).not_to be_valid
  end

  it 'is invalid with no type' do
    expect(build(:sate_chmgt_lookup, type: nil)).not_to be_valid
  end

  it 'is invalid with duplicate name and type' do
    lookup = create(:sate_chmgt_lookup)
    expect(build(:sate_chmgt_lookup, :name => lookup.name)).not_to be_valid
  end
end
