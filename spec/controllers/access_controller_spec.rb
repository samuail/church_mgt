require 'rails_helper'

RSpec.describe AccessController, type: :controller do
  describe "POST #login" do
    let!(:user) { create(:sate_auth_user) }
    let!(:inactive_user) { create(:sate_auth_user, active: false) }
    let(:headers) { valid_headers.except('Authorization') }

    context 'When request is valid' do
      before :each do
        post :login, params: { email: user.email,
                               password: user.password }
        @decoded_response = JSON(@response.body)
      end

      it 'returns an authentication token' do
        expect(@decoded_response["data"]["access_token"]).not_to be_nil
      end

      it "returns a success and success message" do
        expect(@response).to be_successful
        expect(@decoded_response["success"]).to eq true
        expect(@decoded_response["message"]).to eq "Login Successful"
      end
    end

    context "When user password is invalid" do
      before :each do
        post :login, params: { email: user.email,
                               password: user.password + "1" }
        @decoded_response = JSON(@response.body)
      end

      it "returns error message -- Invalid Credentials." do
        expect(@decoded_response["success"]).to eq false
        expect(@decoded_response["errors"]).to eq "Invalid Credentials."
      end
    end

    context "When user is inactive" do
      before :each do
        post :login, params: { email: inactive_user.email,
                               password: inactive_user.password }
        @decoded_response = JSON(@response.body)
      end

      it "returns error message -- Inactive Credentials." do
        expect(@decoded_response["success"]).to eq false
        expect(@decoded_response["errors"]).to eq "Inactive Credentials."
      end
    end

    context "When user is non existing" do
      before :each do
        post :login, params: { email: user.email + "1",
                               password: user.password }
        @decoded_response = JSON(@response.body)
      end

      it "returns error message -- Credentials doesn't Exist." do
        #byebug
        expect(@decoded_response["success"]).to eq false
        expect(@decoded_response["errors"]).to eq "Credentials doesn't Exist."
      end
    end

  end

  describe "GET #logout" do
    let(:user) { create(:sate_auth_user) }
    let(:header) { { 'Authorization' => token_generator(user.id) } }
    let(:expired_header) { { 'Authorization' => expired_token_generator(user.id) } }
    let(:invalid_header) { { 'Authorization' => token_generator(5) } }
    let(:fake_header) { { 'Authorization' => 'foobar' } }

    context "With valid authentication token" do
      before :each do
        request.headers.merge!(header)
        get :logout
        @decoded_response = JSON(@response.body)
      end

      it "returns success" do
        expect(@decoded_response["success"]).to eq true
      end

      it "returns success message -- Logout Successful" do
        expect(@decoded_response["message"]).to eq "Logout Successful"
      end

    end

    context "With no authentication token" do
      before :each do
        get :logout
        @decoded_response = JSON(@response.body)
      end

      it "returns error message -- Missing token" do
        expect(@decoded_response["errors"]).to eq "Missing token"
      end
    end

    context "With expired authentication token" do
      before :each do
        request.headers.merge!(expired_header)
        get :logout
        @decoded_response = JSON(@response.body)
      end

      it "returns error message -- Signature has expired" do
        expect(@decoded_response["errors"]).to eq "Signature has expired"
      end
    end

    context "With invalid authentication token" do
      before :each do
        request.headers.merge!(invalid_header)
        get :logout
        @decoded_response = JSON(@response.body)
      end

      it "returns error message -- Invalid token" do
        expect(@decoded_response["errors"]).to eq "Invalid token Couldn't find Sate::Auth::User with 'id'=5"
      end
    end

    context "With fake authentication token" do
      before :each do
        request.headers.merge!(fake_header)
        get :logout
        @decoded_response = JSON(@response.body)
      end

      it "returns error message -- Decode Error" do
        expect(@decoded_response["errors"]).to eq "Not enough or too many segments"
      end
    end
  end
end