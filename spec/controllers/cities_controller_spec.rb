require 'rails_helper'

RSpec.describe CitiesController, type: :controller do
  include LookupSpec

  let(:user) { create(:sate_auth_user) }
  let!(:application_module) { create(:sate_auth_application_module, code: "CHMS") }
  let(:header) { { 'Authorization' => token_generator(user.id) } }

  let(:valid_attributes) {
    {
        name: FFaker::Name.name
    }
  }

  let(:invalid_attributes) {
    {
        name: ''
    }
  }

  describe 'GET #index' do
    it 'gets all cities' do
      get_index
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new city' do
        post_valid
      end

      it 'should return a success message' do
        post_message
      end
    end

    context 'with invalid params' do
      it 'should return an error message' do
        post_invalid
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) {
        {
            name: FFaker::Name.name
        }
      }

      it 'updates the requested city' do
        put_valid
      end

      it 'should return a success message' do
        put_message
      end
    end

    context 'with invalid params' do
      it 'should return an error message' do
        put_invalid
      end
    end
  end
end
