module LookupSpec
  extend ActiveSupport::Concern

  def get_model_name
    name = self.class.to_s.split('::')
               .select{|s| s.include? 'Controller'}.first
    name.slice!('Controller')
    name.singularize
  end

  def get_humanized
    get_model_name.underscore.humanize
  end

  def get_model_sym
    "SateChmgt#{get_model_name}".underscore.to_sym
  end

  def get_model
    "Sate::Chmgt::#{get_model_name}".constantize
  end

  def get_index
    2.times { create(get_model_sym, application_module_id: application_module.id) }
    request.headers.merge!(header)
    get :index
    result = JSON(response.body)
    expect(result['data'].count).to eq 2
  end

  def post_valid
    request.headers.merge!(header)
    expect {
      post :create, params: {lookup: valid_attributes}, format: :json
    }.to change(get_model, :count).by(1)
  end

  def post_message
    request.headers.merge!(header)
    post :create, params: {lookup: valid_attributes}, format: :json
    result = JSON(response.body)
    expect(result['message']).to eq("#{get_humanized} Saved Successfully!")
  end

  def post_invalid
    request.headers.merge!(header)
    post :create, params: {lookup: invalid_attributes}, format: :json
    result = JSON(response.body)
    expect(result['errors']).to include("#{get_humanized} Name can't be blank")
  end

  def put_valid
    request.headers.merge!(header)
    lookup = create(get_model_sym)
    old_data = lookup.name
    put :update, params: {id: lookup.to_param, lookup: new_attributes}, format: :json
    lookup.reload
    expect(lookup.name).not_to eq old_data
  end

  def put_message
    request.headers.merge!(header)
    lookup = create(get_model_sym)
    put :update, params: {id: lookup.to_param, lookup: valid_attributes}, format: :json
    result = JSON(response.body)
    expect(result['message']).to eq("#{get_humanized} Updated Successfully!")
  end

  def put_invalid
    request.headers.merge!(header)
    lookup = create(get_model_sym)
    put :update, params: {id: lookup.to_param, lookup: invalid_attributes}, format: :json
    result = JSON(response.body)
    expect(result['errors']).to include("#{get_humanized} Name can't be blank")
  end

end