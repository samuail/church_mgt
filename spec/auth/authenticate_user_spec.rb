require 'rails_helper'

RSpec.describe AuthenticateUser do
  let(:user) { create(:sate_auth_user) }
  let(:user_inactive) { create(:sate_auth_user, active: false) }

  subject(:valid_auth_obj) { described_class.new(user.email, user.password) }
  subject(:invalid_auth_password) { described_class.new(user.email,
                                                        user.password + "1") }
  subject(:inactive_auth_obj) { described_class.new(user_inactive.email,
                                                    user_inactive.password) }
  subject(:invalid_auth_obj) { described_class.new('foo', 'bar') }

  describe '#call' do
    context 'when valid credentials' do
      it 'returns an auth token' do
        token = valid_auth_obj.call
        expect(token).not_to be_nil
      end
    end

    context 'when invalid credentials' do
      it 'raises an authentication error -- Invalid Credentials.' do
        expect { invalid_auth_password.call }
            .to raise_error(
                    ExceptionHandler::AuthenticationError,
                    /Invalid Credentials./
                )
      end
    end

    context 'when inactive credentials' do
      it 'raises an authentication error -- Inactive Credentials.' do
        expect { inactive_auth_obj.call }
            .to raise_error(
                    ExceptionHandler::AuthenticationError,
                    /Inactive Credentials./
                )
      end
    end

    context 'when unavailable credentials' do
      it 'raises an authentication error -- Credentials does not Exist.' do
        expect { invalid_auth_obj.call }
            .to raise_error(
                    ActiveRecord::RecordNotFound,
                    /Credentials doesn't Exist./
                )
      end
    end
  end
end