Rails.application.routes.draw do
  mount Sate::Auth::Engine => '/chmgt/auth', as: 'sate_auth'

  post '/chmgt/auth/login', to: 'access#login'
  get '/chmgt/auth/logout', to: 'access#logout'

  get '/chmgt/settings/country', :controller => 'countries', :action => 'index'
  post '/chmgt/settings/country', :controller => 'countries', :action => 'create'
  put '/chmgt/settings/country', :controller => 'countries', :action => 'update'

  get '/chmgt/settings/region', :controller => 'regions', :action => 'index'
  post '/chmgt/settings/region', :controller => 'regions', :action => 'create'
  put '/chmgt/settings/region', :controller => 'regions', :action => 'update'

  get '/chmgt/settings/city', :controller => 'cities', :action => 'index'
  post '/chmgt/settings/city', :controller => 'cities', :action => 'create'
  put '/chmgt/settings/city', :controller => 'cities', :action => 'update'

  get '/chmgt/settings/church', :controller => 'churches', :action => 'index'
  post '/chmgt/settings/church', :controller => 'churches', :action => 'create'
  put '/chmgt/settings/church', :controller => 'churches', :action => 'update'
end
