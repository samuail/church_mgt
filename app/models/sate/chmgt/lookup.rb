class Sate::Chmgt::Lookup < ApplicationRecord
  validates :name, :type, presence: true, uniqueness: { scope: [:name, :type] }

  belongs_to :application_module, class_name: 'Sate::Auth::ApplicationModule'
end
