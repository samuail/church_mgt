class AuthenticateUser
  prepend SimpleCommand

  def initialize(email, password)
    @email = email
    @password = password
  end

  def call
    JsonWebToken.encode(user_id: user.id) if user
  end

  private

  attr_accessor :email, :password

  def user
    user = Sate::Auth::User.find_by_email(email)
    if user
      if user.active
        if user.authenticate(password)
          return user
        else
          raise ExceptionHandler::AuthenticationError, 'Invalid Credentials.'
        end
      else
        raise ExceptionHandler::AuthenticationError, 'Inactive Credentials.'
      end
    else
      raise ActiveRecord::RecordNotFound, "Credentials doesn't Exist."
    end
  end
end