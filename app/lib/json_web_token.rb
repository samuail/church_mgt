class JsonWebToken

  class << self

    CHMS_SECRET = Rails.application.credentials[:church_mgt][:api_audience]

    def encode (payload, exp = 4.hours.from_now)
      payload[:exp] = exp.to_i
      JWT.encode(payload, CHMS_SECRET)
    end

    def decode (token)
      body = JWT.decode(token, CHMS_SECRET)[0]
      HashWithIndifferentAccess.new body

    rescue JWT::ExpiredSignature, JWT::VerificationError => e
      raise ExceptionHandler::ExpiredSignature, e.message
    rescue JWT::DecodeError, JWT::VerificationError => e
      raise ExceptionHandler::DecodeError, e.message
    end

  end

end