require 'sate/common/methodresponse'
require 'sate/common/util'

class ApplicationController < ActionController::API
  include ExceptionHandler
  include ApplicationHelper

  before_action :authorize_request
  attr_reader :current_user

  private

  def authorize_request
    @current_user = AuthorizeApiRequest.call(request.headers).result
  end

end
