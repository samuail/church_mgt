class AccessController < ApplicationController
  skip_before_action :authorize_request, only: %i[login]

  def login
    response = authenticate params[:email], params[:password]
    if response != nil
      render json: response
    end
  end

  def logout
    response = Sate::Common::MethodResponse.new(
        true, 'Logout Successful', {access_token: ""}, nil, 0)
    render json: response
  end

  private

  def authenticate(email, password)
    command = AuthenticateUser.call(email, password)
    response = nil
    if command.success?
      decoded_auth_token = JsonWebToken.decode(command.result)
      user = Sate::Auth::User.find(decoded_auth_token[:user_id])
      roles = user.roles
      response = Sate::Common::MethodResponse.new(
          true, 'Login Successful', { access_token: command.result,
                                      user: user,
                                      roles: roles }, nil, 0)
    end
    response
  end
end
