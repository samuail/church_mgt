module LookupCommon
  extend ActiveSupport::Concern

  included do
    before_action :set_lookup, only: [:update]
  end

  def index
    clazz = get_model
    data = clazz.where(application_module_id: app_module.id).order(:name)
    total = data.count
    response = Sate::Common::MethodResponse.new(true, nil, data, nil, total)
    render json: response
  end

  def create
    clazz = get_model
    obj = clazz.new(lookup_params)
    obj.application_module_id = app_module.id
    class_name = get_model_name.split('::').last.underscore.humanize
    if obj.save
      response = Sate::Common::MethodResponse.new(true, "#{class_name} Saved Successfully!", obj, nil, nil)
    else
      errors = Sate::Common::Util.error_messages obj, class_name
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def update
    class_name = get_model_name.split('::').last.underscore.humanize
    if @lookup.update(lookup_params)
      response = Sate::Common::MethodResponse.new(true, "#{class_name} Updated Successfully!", @lookup, nil, nil)
    else
      errors = Sate::Common::Util.error_messages @lookup, class_name
      response = Sate::Common::MethodResponse.new(false, nil, nil, errors, nil)
    end
    render json: response
  end

  def get_model
    "Sate::Chmgt::#{get_model_name}".constantize
  end

  def get_model_name
    name = self.class.to_s
    name.slice!('Controller')
    name.singularize
  end

  private

  def set_lookup
    clazz = get_model
    @lookup = clazz.find(params[:id])
  end

  def lookup_params
    params.require(:lookup).permit(:name)
  end
end