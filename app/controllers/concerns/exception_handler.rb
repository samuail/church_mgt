require 'sate/common/methodresponse'
module ExceptionHandler
  extend ActiveSupport::Concern

  class AuthenticationError < StandardError; end
  class MissingToken < StandardError; end
  class InvalidToken < StandardError; end
  class ExpiredSignature < StandardError; end
  class DecodeError < StandardError; end

  included do
    rescue_from ActiveRecord::RecordInvalid, with: :four_twenty_two
    rescue_from ExceptionHandler::AuthenticationError, with: :unauthorized_request
    rescue_from ExceptionHandler::MissingToken, with: :four_twenty_two
    rescue_from ExceptionHandler::InvalidToken, with: :four_twenty_two
    rescue_from ExceptionHandler::ExpiredSignature, with: :four_ninety_eight
    rescue_from ExceptionHandler::DecodeError, with: :four_zero_one

    rescue_from ActiveRecord::RecordNotFound do |e|
      response = Sate::Common::MethodResponse. new false, nil, nil, e.message, nil
      render json: response, status: :not_found
    end

    rescue_from ActiveRecord::RecordInvalid do |e|
      response = Sate::Common::MethodResponse. new false, nil, nil, e.message, nil
      render json: response, status: :unprocessable_entity
    end
  end

  private

  # JSON response with message; Status code 422 - unprocessable entity
  def four_twenty_two(e)
    response = Sate::Common::MethodResponse. new false, nil, nil, e.message, nil
    render json: response, status: :unprocessable_entity
  end

  # JSON response with message; Status code 401 - Unauthorized
  def four_ninety_eight(e)
    response = Sate::Common::MethodResponse. new false, nil, nil, e.message, nil
    render json: response
  end

  # JSON response with message; Status code 401 - Unauthorized
  def four_zero_one(e)
    response = Sate::Common::MethodResponse. new false, nil, nil, e.message, nil
    render json: response
  end

  # JSON response with message; Status code 401 - Unauthorized
  def unauthorized_request(e)
    response = Sate::Common::MethodResponse. new false, nil, nil, e.message, nil
    render json: response, status: :unauthorized
  end
end