Feature: Manage Church information
  As a system administrator
  I want to manage church information
  So that the system users can be able to assign church to their address.

  Background:
    Given I have the following application module information
      |  application_name   | code |
      |  Church Management  | ChMS |
    And I have the following user information
      | first_name | last_name |    email    |  password  |  application_name   |
      | Samuel     | Teshome   | sam@abc.com | myPassword |  Church Management  |
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"

  @register_church
  Scenario: Add "Church" information to the application
    When I want to add "church" with the following details
      |	   Name	      |
      |  St. Michael  |
    Then I should see a success message "Church" " Saved Successfully!"
    And I should have "1" "church" detail saved to the system.

  @register_existing_church
  Scenario: Add existing Church information to the application
    And I have the following "church" detail
      |	   Name	     |
      | St. Michael  |
    When I want to add "church" with the following details
      |	   Name	     |
      | St. Michael  |
    Then I should see an error message "Church" " Name has already been taken" and "Church" " Type has already been taken"
    And the total number of "church" detail should only be "1".

  @update_church
  Scenario: Edit "Church" information to the application
    And I have the following "church" detail
      |	   Name	     |
      | St. Michael  |
    When I want to edit "church" by the name "St. Michael" with the following details
      |	   Name	     |
      |  St. Raguel  |
    Then I should see a success message "Church" " Updated Successfully!"
    And I should have "1" "church" detail by the name "St. Raguel".

  @update_with_existing_church
  Scenario: Edit with existing "Church" information to the application
    And I have the following "church" detail
      |	   Name	     |
      | St. Michael  |
    And I have the following "church" detail
      |	   Name	     |
      |  St. Raguel  |
    When I want to edit "church" by the name "St. Michael" with the following details
      |	   Name	     |
      |  St. Raguel  |
    Then I should see an error message "Church" " Name has already been taken" and "Church" " Type has already been taken"

  @list_all_Church
  Scenario: List all "Church" information
    And I have the following "church" detail
      |	   Name	     |
      | St. Michael  |
    And I have the following "church" detail
      |	   Name	     |
      |  St. Raguel  |
    When I want to see all "church" information
    Then I should have the following "2" "church" informations
      |	   Name	     |
      | St. Michael  |
      | St. Raguel   |
