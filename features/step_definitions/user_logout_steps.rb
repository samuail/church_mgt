Given("I am logged in to the system with email {string} and password {string}") do |email, password|
  login_path = "/chmgt/auth/login"
  @response = post login_path, { 'email' => email,
                                 'password' => password }
  @response = JSON(@response.body)
  token = @response["data"]["access_token"]
  header 'Authorization', "Bearer #{token}"
end

When("I want to logout of the system an authorized user") do
  logout_path = "/chmgt/auth/logout"
  token = @response["data"]["access_token"]

  header 'Authorization', "Bearer #{token}"

  @response = get logout_path

  @response = JSON(@response.body)
end

Then("I should be logged out of the system and see the message {string}") do |message|
  puts @response
end

When("I want to logout of the system unauthorized user") do
  logout_path = "/chmgt/auth/logout"
  @response = get logout_path
  @response = JSON(@response.body)
end
