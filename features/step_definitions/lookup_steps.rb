When("I want to add {string} with the following details") do |clazz, data|
  path = "/chmgt/settings/#{clazz}"
  @response = post path, { :lookup => { name: data.raw[1][0] } }
  @response = JSON(@response.body)
end

Then("I should see a success message {string} {string}") do |clazz, message|
  success_message = clazz + message
  expect(@response["message"]).to eq success_message
end

Then("I should have {string} {string} detail saved to the system.") do |count, clazz|
  path = "/chmgt/settings/#{clazz}"
  @response = get path
  @response = JSON(@response.body)
  expect(@response["data"].length).to eq count.to_i
end

Given("I have the following {string} detail") do |clazz, data|
  model = "Sate::Chmgt::#{clazz.underscore.humanize}".constantize
  model.create name: data.raw[1][0], application_module_id: @app_module.id
end

Then("I should see an error message {string} {string} and {string} {string}") do
|clazz, message1, clazz2, message2|
  message = []
  message.push(clazz + message1)
  message.push(clazz2 + message2)
  expect(@response["errors"]).to eq message
end

Then("the total number of {string} detail should only be {string}.") do |clazz, count|
  model = "Sate::Chmgt::#{clazz.underscore.humanize}".constantize
  expect(model.all.count).to eq count.to_i
end

When("I want to edit {string} by the name {string} with the following details") do
|clazz, countryName, newCountry|
  model = "Sate::Chmgt::#{clazz.underscore.humanize}".constantize
  path = "/chmgt/settings/#{clazz}"
  update_clazz = model.find_by_name countryName
  @response = put path, { :id => update_clazz.id, :lookup => { name: newCountry.raw[1][0] } }
  @response = JSON(@response.body)
end

Then("I should have {string} {string} detail by the name {string}.") do |count, clazz, countryName|
  model = "Sate::Chmgt::#{clazz.underscore.humanize}".constantize
  country = model.all
  country2 = model.find_by_name countryName
  expect(country.count).to eq count.to_i
  expect(country2.name).to eq countryName
end

When("I want to see all {string} information") do |clazz|
  path = "/chmgt/settings/#{clazz}"
  @response = get path
  @response = JSON(@response.body)
end

Then("I should have the following {string} {string} informations") do |count, clazz, clazzs|
  expect(@response["data"].count).to eq count.to_i
  expect(@response["data"][0]["name"]).to eq clazzs.raw[1][0]
  expect(@response["data"][1]["name"]).to eq clazzs.raw[2][0]
end
