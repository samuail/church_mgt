Given("I have the following application module information") do |app_module|
  @app_module = Sate::Auth::ApplicationModule.create code: app_module.raw[1][1],
                                                     name: app_module.raw[1][0]
end

Given("I have the following user information") do |user|
  @user = Sate::Auth::User.create first_name: user.raw[1][0],
                                  last_name: user.raw[1][1],
                                  email: user.raw[1][2],
                                  password: user.raw[1][3],
                                  application_module_id: @app_module.id
end

When("I login to the system with email {string} and password {string}") do |email, password|
  login_path = "/chmgt/auth/login"
  @response = post login_path, { 'email' => email,
                                 'password' => password }
  @response = JSON(@response.body)
end

Then("I should see a success message {string}") do |message|
  expect(@response["message"]).to eq message
end

Then("I should see my full name {string}") do |full_name|
  name = @response["data"]["user"]["first_name"] + " " + @response["data"]["user"]["last_name"]
  expect(name).to eq full_name
end

Then("I should see an error message {string}") do |message|
  expect(@response["errors"]).to eq message
end

Given("I have inactive user called {string}") do |user|
  user = Sate::Auth::User.where("first_name = ? and application_module_id = ?",
                                user[0...6], @app_module.id)
  user.first.active = false
  user.first.save
end