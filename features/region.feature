Feature: Manage Region information
  As a system administrator
  I want to manage region information
  So that the system users can be able to assign region to their address.

  Background:
    Given I have the following application module information
      |  application_name   | code |
      |  Church Management  | ChMS |
    And I have the following user information
      | first_name | last_name |    email    |  password  |  application_name   |
      | Samuel     | Teshome   | sam@abc.com | myPassword |  Church Management  |
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"

  @register_region
  Scenario: Add "Region" information to the application
    When I want to add "region" with the following details
      |	  Name	 |
      |  Amhara  |
    Then I should see a success message "Region" " Saved Successfully!"
    And I should have "1" "region" detail saved to the system.

  @register_existing_region
  Scenario: Add existing Region information to the application
    And I have the following "region" detail
      |	  Name	|
      | Amhara  |
    When I want to add "region" with the following details
      |	  Name	|
      | Amhara  |
    Then I should see an error message "Region" " Name has already been taken" and "Region" " Type has already been taken"
    And the total number of "region" detail should only be "1".

  @update_region
  Scenario: Edit "Region" information to the application
    And I have the following "region" detail
      |	  Name	|
      | Amhara  |
    When I want to edit "region" by the name "Amhara" with the following details
      |	  Name	|
      | Oromia  |
    Then I should see a success message "Region" " Updated Successfully!"
    And I should have "1" "region" detail by the name "Oromia".

  @update_with_existing_region
  Scenario: Edit with existing "Region" information to the application
    And I have the following "region" detail
      |	  Name	|
      | Amhara  |
    And I have the following "region" detail
      |	  Name	|
      | Oromia  |
    When I want to edit "region" by the name "Amhara" with the following details
      |	  Name	|
      | Oromia  |
    Then I should see an error message "Region" " Name has already been taken" and "Region" " Type has already been taken"

  @list_all_Region
  Scenario: List all "Region" information
    And I have the following "region" detail
      |	  Name	|
      | Amhara  |
    And I have the following "region" detail
      |	  Name	|
      | Oromia  |
    When I want to see all "region" information
    Then I should have the following "2" "region" informations
      |	  Name	|
      | Amhara  |
      | Oromia  |