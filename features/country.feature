Feature: Manage Country information
  As a system administrator
  I want to manage country information
  So that the system users can be able to assign country to their address.

  Background:
    Given I have the following application module information
      |  application_name   | code |
      |  Church Management  | ChMS |
    And I have the following user information
      | first_name | last_name |    email    |  password  |  application_name   |
      | Samuel     | Teshome   | sam@abc.com | myPassword |  Church Management  |
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"

  @register_country
  Scenario: Add "Country" information to the application
    When I want to add "country" with the following details
      |	  Name	  |
      | Ethiopia  |
    Then I should see a success message "Country" " Saved Successfully!"
    And I should have "1" "country" detail saved to the system.

  @register_existing_country
  Scenario: Add existing Country information to the application
    And I have the following "country" detail
      |	  Name	  |
      | Ethiopia  |
    When I want to add "country" with the following details
      |	  Name	  |
      | Ethiopia  |
    Then I should see an error message "Country" " Name has already been taken" and "Country" " Type has already been taken"
    And the total number of "country" detail should only be "1".

  @update_country
  Scenario: Edit "Country" information to the application
    And I have the following "country" detail
      |	  Name	|
      |  Kenya	|
    When I want to edit "country" by the name "Kenya" with the following details
      |	  Name	  |
      | Ethiopia  |
    Then I should see a success message "Country" " Updated Successfully!"
    And I should have "1" "country" detail by the name "Ethiopia".

  @update_with_existing_country
  Scenario: Edit with existing "Country" information to the application
    And I have the following "country" detail
      |	  Name	  |
      |  Kenya	  |
    And I have the following "country" detail
      |	  Name	  |
      | Ethiopia  |
    When I want to edit "country" by the name "Kenya" with the following details
      |	  Name	  |
      | Ethiopia  |
    Then I should see an error message "Country" " Name has already been taken" and "Country" " Type has already been taken"

  @list_all_Country
  Scenario: List all "Country" information
    And I have the following "country" detail
      |	  Name	  |
      |  Kenya	  |
    And I have the following "country" detail
      |	  Name	  |
      | Ethiopia  |
    When I want to see all "country" information
    Then I should have the following "2" "country" informations
      |	  Name	  |
      | Ethiopia  |
      |  Kenya	  |
