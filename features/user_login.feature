Feature: Login User to the application.
  As a system user
  I want to login into the system
  So that i can access the system activities

  Background:
    Given I have the following application module information
      |  application_name   | code |
      |  Church Management  | ChMS |
    And I have the following user information
      | first_name | last_name |    email    |  password  |  application_name   |
      | Samuel     | Teshome   | sam@abc.com | myPassword |  Church Management  |

  @login_user
  Scenario: Login user to the application
    When I login to the system with email "sam@abc.com" and password "myPassword"
    Then I should see a success message "Login Successful"
    Then I should see my full name "Samuel Teshome"

  @login_user_wrong_email
  Scenario: Login unregistered user to the application
    When I login to the system with email "sam1@abc.com" and password "myPassword"
    Then I should see an error message "Credentials doesn't Exist."

  @login_user_wrong_password
  Scenario: Login user with wrong password to the application
    When I login to the system with email "sam@abc.com" and password "mypassword"
    Then I should see an error message "Invalid Credentials."

  @login_inactive_user
  Scenario: Login inactive user to the application
    And I have inactive user called "Samuel Teshome"
    When I login to the system with email "sam@abc.com" and password "myPassword"
    Then I should see an error message "Inactive Credentials."
