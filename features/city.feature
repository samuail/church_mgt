Feature: Manage City information
  As a system administrator
  I want to manage city information
  So that the system users can be able to assign city to their address.

  Background:
    Given I have the following application module information
      |  application_name   | code |
      |  Church Management  | ChMS |
    And I have the following user information
      | first_name | last_name |    email    |  password  |  application_name   |
      | Samuel     | Teshome   | sam@abc.com | myPassword |  Church Management  |
    And I am logged in to the system with email "sam@abc.com" and password "myPassword"

  @register_city
  Scenario: Add "City" information to the application
    When I want to add "city" with the following details
      |	   Name	      |
      |  Addis Ababa  |
    Then I should see a success message "City" " Saved Successfully!"
    And I should have "1" "city" detail saved to the system.

  @register_existing_city
  Scenario: Add existing City information to the application
    And I have the following "city" detail
      |	   Name	     |
      | Addis Ababa  |
    When I want to add "city" with the following details
      |	   Name	     |
      | Addis Ababa  |
    Then I should see an error message "City" " Name has already been taken" and "City" " Type has already been taken"
    And the total number of "city" detail should only be "1".

  @update_city
  Scenario: Edit "City" information to the application
    And I have the following "city" detail
      |	   Name	     |
      | Addis Ababa  |
    When I want to edit "city" by the name "Addis Ababa" with the following details
      |	   Name	     |
      |  Dire Dawa   |
    Then I should see a success message "City" " Updated Successfully!"
    And I should have "1" "city" detail by the name "Dire Dawa".

  @update_with_existing_city
  Scenario: Edit with existing "City" information to the application
    And I have the following "city" detail
      |	   Name	     |
      | Addis Ababa  |
    And I have the following "city" detail
      |	   Name	     |
      |  Dire Dawa   |
    When I want to edit "city" by the name "Addis Ababa" with the following details
      |	   Name	     |
      |  Dire Dawa   |
    Then I should see an error message "City" " Name has already been taken" and "City" " Type has already been taken"

  @list_all_City
  Scenario: List all "City" information
    And I have the following "city" detail
      |	   Name	     |
      | Addis Ababa  |
    And I have the following "city" detail
      |	   Name	     |
      |  Dire Dawa   |
    When I want to see all "city" information
    Then I should have the following "2" "city" informations
      |	   Name	     |
      | Addis Ababa  |
      |  Dire Dawa   |
