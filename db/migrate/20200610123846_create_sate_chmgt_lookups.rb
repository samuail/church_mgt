class CreateSateChmgtLookups < ActiveRecord::Migration[6.0]
  def change
    create_table :sate_chmgt_lookups do |t|
      t.string :name, null: false
      t.string :type, null: false
      t.references :application_module, index: true

      t.timestamps
    end
    add_index :sate_chmgt_lookups, [:name, :type], { :unique => true }
    add_foreign_key :sate_chmgt_lookups, :sate_auth_application_modules,
                    column: :application_module_id
  end
end
