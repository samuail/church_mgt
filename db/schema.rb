# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_06_10_123846) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "sate_auth_application_modules", force: :cascade do |t|
    t.string "code", limit: 6, null: false
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["code"], name: "index_sate_auth_application_modules_on_code", unique: true
  end

  create_table "sate_auth_menus", force: :cascade do |t|
    t.string "text", limit: 50, null: false
    t.string "icon_cls"
    t.string "class_name"
    t.string "location"
    t.integer "parent_id"
    t.bigint "application_module_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["application_module_id"], name: "index_sate_auth_menus_on_application_module_id"
    t.index ["parent_id"], name: "index_sate_auth_menus_on_parent_id"
    t.index ["text", "application_module_id"], name: "index_sate_auth_menus_on_text_and_application_module_id", unique: true
  end

  create_table "sate_auth_user_menus", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "menu_id"
    t.index ["menu_id"], name: "index_sate_auth_user_menus_on_menu_id"
    t.index ["user_id", "menu_id"], name: "index_user_menu_are_unique", unique: true
    t.index ["user_id"], name: "index_sate_auth_user_menus_on_user_id"
  end

  create_table "sate_auth_user_roles", force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.bigint "application_module_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["application_module_id"], name: "index_sate_auth_user_roles_on_application_module_id"
    t.index ["name", "application_module_id"], name: "index_sate_auth_user_roles_on_name_and_application_module_id", unique: true
  end

  create_table "sate_auth_user_user_roles", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "user_role_id"
    t.index ["user_id", "user_role_id"], name: "index_user_user_role_are_unique", unique: true
    t.index ["user_id"], name: "index_sate_auth_user_user_roles_on_user_id"
    t.index ["user_role_id"], name: "index_sate_auth_user_user_roles_on_user_role_id"
  end

  create_table "sate_auth_users", force: :cascade do |t|
    t.string "first_name", limit: 50, null: false
    t.string "last_name", limit: 50, null: false
    t.string "email", limit: 50, null: false
    t.string "password_digest", null: false
    t.boolean "active", default: true, null: false
    t.bigint "application_module_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["application_module_id"], name: "index_sate_auth_users_on_application_module_id"
    t.index ["email"], name: "index_sate_auth_users_on_email", unique: true
  end

  create_table "sate_chmgt_lookups", force: :cascade do |t|
    t.string "name", null: false
    t.string "type", null: false
    t.bigint "application_module_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["application_module_id"], name: "index_sate_chmgt_lookups_on_application_module_id"
    t.index ["name", "type"], name: "index_sate_chmgt_lookups_on_name_and_type", unique: true
  end

  add_foreign_key "sate_auth_menus", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_auth_menus", "sate_auth_menus", column: "parent_id"
  add_foreign_key "sate_auth_user_menus", "sate_auth_menus", column: "menu_id"
  add_foreign_key "sate_auth_user_menus", "sate_auth_users", column: "user_id"
  add_foreign_key "sate_auth_user_roles", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_auth_user_user_roles", "sate_auth_user_roles", column: "user_role_id"
  add_foreign_key "sate_auth_user_user_roles", "sate_auth_users", column: "user_id"
  add_foreign_key "sate_auth_users", "sate_auth_application_modules", column: "application_module_id"
  add_foreign_key "sate_chmgt_lookups", "sate_auth_application_modules", column: "application_module_id"
end
